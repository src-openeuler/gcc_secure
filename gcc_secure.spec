Name:           gcc_secure
Summary:        Enforcing secure compile options for abuild
License:        GPL-2.0-only
Group:          System/Management
Version:        1.0
Release:        0.13
BuildRoot:      %{_tmppath}/%{name}-%{version}

BuildRequires: util-linux coreutils

Requires:  util-linux rpm grep binutils gcc coreutils rpm-build gcc-c++

%description
Enforcing secure compile option for abuild

%prep

%install

%pre

%post
echo -e '*cc1_options:\n+ %{!D__KERNEL__:%{!nostdlib:%{!nodefaultlibs:%{!fno-stack-protector:%{!fstack-protector-all:-fstack-protector-strong}}}}}' >/opt/gcc-specs-fs-cc1

echo -e '*cc1_options:\n+ %{!r:%{!D__KERNEL__:%{!pie:%{!fpic:%{!fPIC:%{!fpie:%{!fPIE:%{!fno-pic:%{!fno-PIC:%{!fno-pie:%{!fno-PIE:%{!shared:%{!static:%{!nostdlib:%{!nostartfiles:-fPIE}}}}}}}}}}}}}}}' >/opt/gcc-specs-pie-cc1

echo -e '*self_spec:\n+ %{!D__KERNEL__:%{!pie:%{!A:%{!fno-pie:%{!fno-PIE:%{!fno-pic:%{!fno-PIC:%{!shared:%{!static:%{!r:%{!nostdlib:%{!nostartfiles:-pie}}}}}}}}}}}}' >/opt/gcc-specs-pie-ld

old_gcc=/usr/bin/gcc
mv $old_gcc $old_gcc"_old"
cat <<END1 > $old_gcc
#!/bin/sh
gcc_secure_exclude=\`rpm --eval %{gcc_secure_exclude}\`
if ! cat /.build.command | egrep "\$gcc_secure_exclude" &>/dev/null; then
    sec_opt='-fPIC -D_FORTIFY_SOURCE=2 -O2 -Wl,-z,relro -Wl,-z,now -Wl,-z,noexecstack -Wtrampolines -fsigned-char'
    fs_opt=''

    if [[ "\$@" =~ "-O0" ]]; then
        #openjdk set O0 can not use FS
        sec_opt=\`echo \$sec_opt | sed 's/ -D_FORTIFY_SOURCE=2 -O2 / /'\`
    fi

    if [ -d '/home/abuild/rpmbuild/SOURCES' ]; then
        configfile=/home/abuild/rpmbuild/SOURCES/config_for_secure
    else
        configfile=/root/rpmbuild/SOURCES/config_for_secure
    fi

    if [ -f \$configfile ]; then
        rpm_name=\`cat \$configfile| grep rpm_name| awk -F: '{print \$NF}'\`
        sec_opt=\`cat \$configfile| grep sec_opt| awk -F: '{print \$NF}'\`
        fs_opt=\`cat \$configfile| grep fs_opt| awk -F: '{print \$NF}'\`
    fi

    if [[ x\$rpm_name = "xnumactl" ]] && [[ "\$@" =~ "-march=x86-64" ]];then
        #numactl i686 use asm can't add -fPIC
        sec_opt=\`echo \$sec_opt | sed 's/-fPIC / /'\`
    fi

    if [[ x\$rpm_name = "xglibc" ]] || [[ x\$rpm_name = "xcompat-glibc" ]];then
        #glibc supply fs define, can not add fs for glibc self
        /usr/bin/gcc_old \$sec_opt "\$@" \$fs_opt --specs=/opt/gcc-specs-pie-cc1 --specs=/opt/gcc-specs-pie-ld
    else
        /usr/bin/gcc_old \$sec_opt "\$@" \$fs_opt --specs=/opt/gcc-specs-pie-cc1 --specs=/opt/gcc-specs-pie-ld --specs=/opt/gcc-specs-fs-cc1
    fi

else
    $old_gcc"_old" "\$@"
fi
END1
chmod 755 $old_gcc $old_gcc"_old"

old_gplus=/usr/bin/g++
if [ -f $old_gplus ]; then
mv $old_gplus $old_gplus"_old"
cat <<END1 > $old_gplus
#!/bin/sh
gcc_secure_exclude=\`rpm --eval %{gcc_secure_exclude}\`
if ! cat /.build.command | egrep "\$gcc_secure_exclude" &>/dev/null; then
    sec_opt='-fPIC -D_FORTIFY_SOURCE=2 -O2 -Wl,-z,relro -Wl,-z,now -Wl,-z,noexecstack -Wtrampolines -fsigned-char'
    fs_opt=''

    if [[ "\$@" =~ "-O0" ]]; then
        #openjdk set O0 can not use FS
        sec_opt=\`echo \$sec_opt | sed 's/ -D_FORTIFY_SOURCE=2 -O2 / /'\`
    fi

    if [ -d '/home/abuild/rpmbuild/SOURCES' ]; then
        configfile=/home/abuild/rpmbuild/SOURCES/config_for_secure_g++
    else
        configfile=/root/rpmbuild/SOURCES/config_for_secure_g++
    fi

    if [ -f \$configfile ]; then
        rpm_name=\`cat \$configfile| grep rpm_name| awk -F: '{print \$NF}'\`
        sec_opt=\`cat \$configfile| grep sec_opt| awk -F: '{print \$NF}'\`
        fs_opt=\`cat \$configfile| grep fs_opt| awk -F: '{print \$NF}'\`
    fi

    if [[ x"\$@" = "x-v" ]];then
        #libtool use g++ -v for test compile env,if add Wl opt, it will make g++ -v fail
        $old_gplus"_old" "\$@"
    else
        /usr/bin/g++_old \$sec_opt "\$@" \$fs_opt  --specs=/opt/gcc-specs-pie-cc1 --specs=/opt/gcc-specs-pie-ld --specs=/opt/gcc-specs-fs-cc1
    fi
else
    $old_gplus"_old" "\$@"
fi
END1
chmod 755 $old_gplus $old_gplus"_old"
fi

old_cpp=/usr/bin/c++
if [ -f $old_cpp ]; then
mv $old_cpp $old_cpp"_old"
cat <<END1 > $old_cpp
#!/bin/sh
gcc_secure_exclude=\`rpm --eval %{gcc_secure_exclude}\`
if ! cat /.build.command | egrep "\$gcc_secure_exclude" &>/dev/null; then
    sec_opt='-fPIC -D_FORTIFY_SOURCE=2 -O2 -Wl,-z,relro -Wl,-z,now -Wl,-z,noexecstack -Wtrampolines -fsigned-char'
    fs_opt=''

    if [[ "\$@" =~ "-O0" ]]; then
        #openjdk set O0 can not use FS
        sec_opt=\`echo \$sec_opt | sed 's/ -D_FORTIFY_SOURCE=2 -O2 / /'\`
    fi

    if [ -d '/home/abuild/rpmbuild/SOURCES' ]; then
        configfile=/home/abuild/rpmbuild/SOURCES/config_for_secure_c++
    else
        configfile=/root/rpmbuild/SOURCES/config_for_secure_c++
    fi

    if [ -f \$configfile ]; then
        rpm_name=\`cat \$configfile| grep rpm_name| awk -F: '{print \$NF}'\`
        sec_opt=\`cat \$configfile| grep sec_opt| awk -F: '{print \$NF}'\`
        fs_opt=\`cat \$configfile| grep fs_opt| awk -F: '{print \$NF}'\`
    fi

    /usr/bin/c++_old \$sec_opt "\$@" \$fs_opt  --specs=/opt/gcc-specs-pie-cc1 --specs=/opt/gcc-specs-pie-ld --specs=/opt/gcc-specs-fs-cc1

else
    $old_cpp"_old" "\$@"
fi
END1
chmod 755 $old_cpp $old_cpp"_old"
fi

%preun

%postun


%files
%defattr(-,root,root)

%clean
rm -rf $RPM_BUILD_ROOT/*
rm -rf %{_tmppath}/%{name}-%{version}
rm -rf $RPM_BUILD_DIR/%{name}-%{version}

%changelog
* Fri Nov 25 2022 Ge Wang<wnagge20@h-partners.com> - 1.0-0.13
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: add requires gcc-c++

* Thu Jul 28 2022 Chenyx <chenyixiong3@huawei.com> - 1.0-0.12
- License compliance rectification

* Thu Feb 17 2022 zoulin<zoulin13@h-partners.com> - 1.0-0.11
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: Add automake.spec to gcc_secure_exclude

* Thu Feb 10 2022 zhangchenfeng<zhangchenfeng1@huawei.com> - 1.0-0.10
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: clean spec

* Wed Jul 21 2021 licihua<licihua@huawei.com> - 1.0-0.9
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: support -fsigned-char for g++

* Mon Jul 12 2021 shenyangyang<shenyangyang4@huawei.com> - 1.0-0.8
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:change file mode from 777 to 755 for secure

* Thu Apr 8 2021 shenyangyang<shenyangyang4@huawei.com> - 1.0-0.7
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: bump release rebuilding for adding rhash.spec to macro gcc_secure_exclude

* Tue Feb 18 2020 yanzhihua<yanzhihua4@huawei.com> - 1.0-0.6
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: add compile options for arm

* Mon Jan 13 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.0-0.5
- package init

* Mon Apr 01 2019 liuxueping<liuxueping1@huawei.com> - 1.0-0.4
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: no buildrequires rpms about tool

* Tue Nov 27 2018 zhangchenfeng<zhangchenfeng1@huawei.com> - 1.0-0.3
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:resolve numactl i686 fail

* Tue Nov 27 2018 zhangchenfeng<zhangchenfeng1@huawei.com> - 1.0-0.2
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:resolve numactl i686 fail

* Sat Nov 10 2018 Wuyou<wuyou88@huawei.com> - 1.0-0.1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:add safe compile options
